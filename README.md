# Photo Memories
Photo memories is a program that allows you to view photos that were taken on this day but on a previous year. Similar to the app Timehop or a feature Facebook has implemented. 

## Status [Updated 2020-10-27]
This project has been abandoned for a long time and may no longer work with newer versions of Windows.

## License
The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
